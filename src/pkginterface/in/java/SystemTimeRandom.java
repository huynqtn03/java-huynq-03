/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkginterface.in.java;

/**
 *
 * @author quang huy
 */
public class SystemTimeRandom implements IRandomer {

    public SystemTimeRandom() {
    }

    @Override
    public int getRandomNumber(int maxValue) {
        return (int) (System.currentTimeMillis() % maxValue);
    }

    
}
