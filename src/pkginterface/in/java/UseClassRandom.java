/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkginterface.in.java;

import java.util.Random;

/**
 *
 * @author quang huy
 */

   
 public class UseClassRandom implements IRandomer{

    @Override
    public int getRandomNumber(int maxValue) {
        Random rand = new Random();
        return rand.nextInt(maxValue);
    }
    
    public int getMaxValue(){
        return 0;
    }
}


