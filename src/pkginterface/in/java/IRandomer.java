/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkginterface.in.java;

/**
 *
 * @author quang huy
 */
public interface IRandomer {
    /**
     * Trả lại giá trị ngẫu nhiên từ 0 đến maxValue.
     * @param maxValue : Giá trị cao nhất (không bao gồm)
     * @return 
     */
    public int getRandomNumber(int maxValue);
}


