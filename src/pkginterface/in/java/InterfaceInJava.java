/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkginterface.in.java;

import java.util.Random;

/**
 *
 * @author quang huy
 */
public class InterfaceInJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        IRandomer useClassRandom = new UseClassRandom();
        System.out.println("Gia tri random tu class random: "
                + useClassRandom.getRandomNumber(200));

        IRandomer useSystemTime = new SystemTimeRandom();
        System.out.println("Gia tri random tu system time: "
                + useSystemTime.getRandomNumber(200));

        IRandomer useSystemTime2 = new IRandomer() { // functional interface
            @Override                                // anonymous class
            public int getRandomNumber(int maxValue) {
                return (int) (System.currentTimeMillis() % maxValue);
            }
        };
        System.out.println("Gia tri random tu anonymous class: "
                + useSystemTime2.getRandomNumber(200));

        IRandomer useSystemTime3 = (int maxValue) -> {       
            return (int) (System.currentTimeMillis() % maxValue); // lambda
        };
        System.out.println("Gia tri random tu lambda: "
                + useSystemTime3.getRandomNumber(200));

    }

}
