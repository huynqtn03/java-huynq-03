/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuanLyNhanVien;

/**
 *
 * @author quang huy
 */
public class QuanLyBanHang extends NhanVienVanPhong implements WorkBenefit{
    private String chungChiNghe;

    public QuanLyBanHang(String chungChiNghe, String totNghiepTruong, String queQuan, int maNhanVien, String tenNhanVien, String ngaySinh) {
        super(totNghiepTruong, queQuan, maNhanVien, tenNhanVien, ngaySinh);
        this.chungChiNghe = chungChiNghe;
    }

    public String getChungChiNghe() {
        return chungChiNghe;
    }

    public void setChungChiNghe(String chungChiNghe) {
        this.chungChiNghe = chungChiNghe;
    }

    @Override
    public String getWorkBenefit() {
        return "Chi tra chi phi di lai.";
    }
    
}
