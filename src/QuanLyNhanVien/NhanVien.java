/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuanLyNhanVien;

import java.util.Date;

/**
 *
 * @author quang huy
 */
public class NhanVien {
    protected final int maNhanVien;
    protected final String tenNhanVien;
    protected final String ngaySinh;

    public NhanVien(int maNhanVien, String tenNhanVien, String ngaySinh) {
        this.maNhanVien = maNhanVien;
        this.tenNhanVien = tenNhanVien;
        this.ngaySinh = ngaySinh;
    }

    public int getMaNhanVien() {
        return maNhanVien;
    }

    public String getTenNhanVien() {
        return tenNhanVien;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }
    
    
    
}

