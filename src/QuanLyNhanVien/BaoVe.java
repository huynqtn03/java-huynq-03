/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuanLyNhanVien;

import java.util.Objects;

/**
 *
 * @author quang huy
 */
public class BaoVe extends NhanVien implements WorkBenefit{
    private String chungChiVoNghe;

    public BaoVe(String chungChiVoNghe, int maNhanVien, String tenNhanVien, String ngaySinh) {
        super(maNhanVien, tenNhanVien, ngaySinh);
        this.chungChiVoNghe = chungChiVoNghe;
    }

    public void setChungChiVoNghe(String chungChiVoNghe) {
        this.chungChiVoNghe = chungChiVoNghe;
    }

    public String getChungChiVoNghe() {
        return chungChiVoNghe;
    }

    @Override
    public String toString() {
        return "BaoVe{" + "chungChiVoNghe=" + chungChiVoNghe + '}';
    }

    @Override
    public String getWorkBenefit() {
        return "Chi tra tien an trua va toi.";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.chungChiVoNghe);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BaoVe other = (BaoVe) obj;
        return Objects.equals(this.chungChiVoNghe, other.chungChiVoNghe);
    }
    
    
    
}
