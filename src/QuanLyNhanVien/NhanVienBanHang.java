/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuanLyNhanVien;

/**
 *
 * @author quang huy
 */
public class NhanVienBanHang extends NhanVienVanPhong implements WorkBenefit{
 
    public NhanVienBanHang(String totNghiepTruong, String queQuan, int maNhanVien, String tenNhanVien, String ngaySinh) {
        super(totNghiepTruong, queQuan, maNhanVien, tenNhanVien, ngaySinh);
    }

    @Override
    public String getWorkBenefit() {
        return "Chi tra tien an trua va dien thoai";
    }
     
}
