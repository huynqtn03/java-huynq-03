/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuanLyNhanVien;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author quang huy
 */
public class baitapthuchanh2 {

    public static void main(String[] args) {
        List<NhanVien> danhSachNhanVien = new ArrayList<>();

        // Thêm nhân viên vào danh sách
        danhSachNhanVien.add(new BaoVe("Dai Den", 1, "Tran van Dinh", "11/1/2005"));
        danhSachNhanVien.add(new NhanVienBanHang("FTU", "Thai Nguyen", 2, "Nguyen Van Hieu", "03/05/2002"));
        danhSachNhanVien.add(new NhanVienKho("TOT", "Sy Quan Đặc Công", "Bắc Ninh", 55, "Xuân Hữu", "05/12/1998"));
        danhSachNhanVien.add(new QuanLyBanHang("QTKD", "Thương Mại", "Hà Nội", 12, "Dương Thùy Linh", "31/10/2003"));
        danhSachNhanVien.add(new BaoVe("Vovinam", 4, "Đặng Thành Trung", "25/12/1995"));
        danhSachNhanVien.add(new NhanVienKho("Khá", "Thể Thao Bắc Ninh", "Bắc Ninh", 9, "Hữu Sỹ", "6/7/2000"));
        danhSachNhanVien.add(new NhanVienBanHang("Kinh Công", "Hòa Bình", 123, "Dương Hưng", "23/1/2003"));
        danhSachNhanVien.add(new NhanVienBanHang("Công Nghiệp", "Cao Bằng", 567, "Phùng Thanh Độ", "12/7/1989"));
        danhSachNhanVien.add(new QuanLyBanHang("Gioi", "Đại học Y", "Tuyên Quang", 82, "Phương Thảo", "12/5/1993"));
        danhSachNhanVien.add(new NhanVienKho("Yếu", "Kinh Tế", "Hải Phòng", 71, "Trần Văn Dũng", "23/1/2000"));
        List<NhanVien> danhSachNhanVienMoi = new ArrayList<>();
//        System.out.println(danhSachNhanVien.size());
//
//        System.out.println(danhSachNhanVien.get(0));
//        System.out.println(danhSachNhanVien.get(1));
        try (Scanner scanner = new Scanner(System.in)) {
            int choice = -1;
            while (choice != 0) {
                displayMenu();
                System.out.print("Nhập lựa chọn của bạn: ");
                try {
                    choice = scanner.nextInt();
                    scanner.nextLine(); // Đọc ký tự thừa sau khi đọc số nguyên

                    switch (choice) {
                        case 1:

                            Question1(danhSachNhanVien);

                            break;
                        case 2:
//                           danhSachNhanVien.removeIf(nhanVien -> nhanVien instanceof BaoVe);
                            Question2(danhSachNhanVien);
                            break;
                        case 3:

                            System.out.println("Nhân viên sinh từ năm 1970 đến năm 2000!");
                            // Lọc và thêm các nhân viên thỏa điều kiện vào danh sách mới
                            for (NhanVien nhanVien : danhSachNhanVien) {
                                int namSinh = Integer.parseInt(nhanVien.getNgaySinh().split("/")[2]);
                                if (namSinh >= 1970 && namSinh <= 2000) {
                                    danhSachNhanVienMoi.add(nhanVien);
                                }
                            }

                            // In danh sách nhân viên mới
                            System.out.println("Thông tin về loại nhân viên trong danh sách mới:");
                            for (NhanVien nhanVien : danhSachNhanVienMoi) {
                                if (nhanVien instanceof BaoVe) {
                                    System.out.println("Nhân viên: " + nhanVien.getTenNhanVien() + " - Loại: Bảo vệ");
                                } else if (nhanVien instanceof NhanVienBanHang) {
                                    System.out.println("Nhân viên: " + nhanVien.getTenNhanVien() + " - Loại: Nhân viên bán hàng");
                                } else if (nhanVien instanceof QuanLyBanHang) {
                                    System.out.println("Nhân viên: " + nhanVien.getTenNhanVien() + " - Loại: Quản lý bán hàng");
                                } else if (nhanVien instanceof NhanVienKho) {
                                    System.out.println("Nhân viên: " + nhanVien.getTenNhanVien() + " - Loại: Nhân viên kho");
                                }
                            }
                            break;
                        case 4:
                            System.out.println("Phúc lợi công việc của danh sách nhân viên mới:");
                            for (NhanVien nhanVien : danhSachNhanVienMoi) {
                                if (nhanVien instanceof WorkBenefit) {
                                    WorkBenefit workBenefit = (WorkBenefit) nhanVien;
                                    System.out.println("Nhân viên: " + nhanVien.getTenNhanVien());
                                    System.out.println("Phúc lợi công việc: " + workBenefit.getWorkBenefit());
                                    System.out.println();
                                }
                            }
                            break;

                        case 0:
                            System.out.println("Kết thúc chương trình");
                            break;
                        default:
                            System.out.println("Lựa chọn không hợp lệ");
                            break;
                    }

                } catch (Exception e) {
                    System.out.println("Lựa chọn không hợp lệ");
                    scanner.nextLine(); // Đọc ký tự thừa sau khi xảy ra lỗi
                }

                System.out.println();
            }
        }

    }

    public static void Question1(List<NhanVien> danhSachNhanVien) {
        int countBaoVe = 0;
        int countNhanVienBanHang = 0;
        int countQuanLyBanHang = 0;
        int countNhanVienKho = 0;

// Duyệt qua danh sách và tăng giá trị biến đếm tương ứng
        for (NhanVien nhanVien : danhSachNhanVien) {
            if (nhanVien instanceof BaoVe) {
                countBaoVe++;
            } else if (nhanVien instanceof NhanVienBanHang) {
                countNhanVienBanHang++;
            } else if (nhanVien instanceof QuanLyBanHang) {
                countQuanLyBanHang++;
            } else if (nhanVien instanceof NhanVienKho) {
                countNhanVienKho++;
            }
        }

// In số lượng nhân viên từng loại
        System.out.println("Số lượng nhân viên bảo vệ: " + countBaoVe);
        System.out.println("Số lượng nhân viên bán hàng: " + countNhanVienBanHang);
        System.out.println("Số lượng quản lý bán hàng: " + countQuanLyBanHang);
        System.out.println("Số lượng nhân viên kho: " + countNhanVienKho);

    }

    public static void Question2(List<NhanVien> danhSachNhanVien) {
        danhSachNhanVien.removeIf(nhanVien -> nhanVien instanceof BaoVe);
        System.out.println("Đã xóa bảo vệ khỏi danh sách");

        Question1(danhSachNhanVien);

    }
    
     private static void displayMenu() {
        System.out.println("----- MENU -----");
        System.out.println("1. Duyệt và đếm trong danh sách trên có bao nhiêu loại nhân viên ");
        System.out.println("2. Xóa nhân viên bảo vệ khỏi danh sách");
        System.out.println("3. Tạo một danh sách nhân viên mới với các nhân viên từ năm 1970 đến 2000");
        System.out.println("4. gọi hàm getWorkBenefit cho danh sách nhân viên mới");
        System.out.println("0. Kết thúc chương trình");
        System.out.println("\n");

    }
}
