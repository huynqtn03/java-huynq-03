/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuanLyNhanVien;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author quang huy
 */
public class RunnerTest {
//    public static void main(String[] args) {
//        Date vuNgocDate = new Date();
//        Calendar cal= Calendar.getInstance();
//        cal.set(2000, 8,7 );
//        vuNgocDate =cal.getTime();
//        NhanVien vuNgoc=new BaoVe("Dai Den",2,0, "Vu Minh Ngoc", vuNgocDate);
//      
//        System.out.println("Hiệu quả công việc của bảo vệ: "+vuNgoc.hieuQuaCongViec());
//        System.out.println("Benefit của Bảo vệ: "+vuNgoc.getWorkBenefit()+"\n");
//       
//        NhanVien vuNgoc2=new NhanVienBanHang(11,2 , "Vu Minh Ngoc 2", vuNgocDate);
//        System.out.println("Hieu qua cong viec cua nhan vien ban hang: "+vuNgoc2.hieuQuaCongViec());
//        System.out.println("Benefit của nhan vien ban hang : "+vuNgoc2.getWorkBenefit()+"\n");
//        
//        NhanVien vuNgoc3=new NhanVienKho(1, 2, 45, "Vu Minh Ngoc 3", vuNgocDate);
//        System.out.println("Hieu qua cong viec cua nhan vien kho: "+vuNgoc3.hieuQuaCongViec());
//        System.out.println("Benefit của Nhan Vien Kho : "+vuNgoc3.getWorkBenefit()+"\n");
//        
//        NhanVien vuNgoc4=new QuanLyBanHang("Nhan Vien lau nam", 6000000, 9, "Vu Minh Ngoc 4", vuNgocDate);
//        System.out.println("Hieu qua cong viec cua Quan Ly Ban Hang : "+vuNgoc4.hieuQuaCongViec());
//        System.out.println("Benefit của Quan Ly Ban Hang : "+vuNgoc4.getWorkBenefit()+"\n");
//    }
//    public static void main(String[] args) {
//    Date vuNgocDate = new Date();
//    Calendar cal= Calendar.getInstance();
//    cal.set(2000, 8, 7);
//    vuNgocDate = cal.getTime();
//    
////    NhanVien[] employees = new NhanVien[4]; // Tạo mảng employees có kích thước 4
////    
////    employees[0] = new BaoVe("Dai Den", 2, 0, "Vu Minh Ngoc", vuNgocDate);
////    employees[1] = new NhanVienBanHang(11, 2, "Vu Minh Ngoc 2", vuNgocDate);
////    employees[2] = new NhanVienKho(1, 2, 45, "Vu Minh Ngoc 3", vuNgocDate);
////    employees[3] = new QuanLyBanHang("Nhan Vien lau nam", 6000000, 9, "Vu Minh Ngoc 4", vuNgocDate);
////    
////    // Sử dụng vòng lặp for để in ra tất cả các phần tử trong mảng Employees
////    for (int i = 0; i < employees.length; i++) {
////        System.out.println("Employees: "+employees[i].toString());
////        System.out.println("Hiệu quả công việc: " + employees[i].hieuQuaCongViec());
////        System.out.println("Benefit: " + employees[i].getWorkBenefit() + "\n");
////    }
//      List<NhanVien> danhSachNhanVien = new ArrayList<>();
      
     public static void main(String[] args) {
        // Khởi tạo ArrayList danhSachMaNhanVien với kích thước ban đầu là 500
        // và chỉ chứa các số nguyên
        List<Integer> danhSachMaNhanVien = new ArrayList<>(500);
        // Khởi tạo ArrayList danhSachBaoVe, là một danh sách với kiểu là đối 
        // tượng thuộc class BaoVe
        List<BaoVe> danhSachBaoVe = new ArrayList<>();
        // Khởi tạo ArrayList danhSachNhanVien, là một danh sách với kiểu là đối 
        // tượng thuộc class cha NhanVien. Dữ liệu được lấy từ danhSachBaoVe
        List<NhanVien> danhSachNhanVien = new ArrayList<>(danhSachBaoVe);
        // Khởi tạo ArrayList danhSachWorkBenefit, là một danh sách với kiểu là đối 
        // tượng thuộc interface WorkBenefit. Dữ liệu được lấy từ danhSachBaoVe
        List<WorkBenefit> danhSachWorkBenefit = new ArrayList<>(danhSachBaoVe);
        
        danhSachMaNhanVien.add(1);
        int maNhanVienA = 56;
        danhSachMaNhanVien.add(maNhanVienA);
        
        BaoVe nhatBaoVe = new BaoVe("Dai Den taekwondo", 2, "Nguyễn Văn Nhật", "10/10/1977");
        danhSachBaoVe.add(nhatBaoVe);
        danhSachNhanVien.add(nhatBaoVe);
        danhSachWorkBenefit.add(nhatBaoVe);
        
        int maNhanVien = danhSachMaNhanVien.get(5);
        BaoVe baoVe = danhSachBaoVe.get(0);
        NhanVien nhanVien = danhSachNhanVien.get(1);
        WorkBenefit benefit = danhSachWorkBenefit.get(maNhanVien);    
        
        BaoVe longBaoVe = new BaoVe("Dai Do taekwondo", 3, "Nguyễn Minh Long", "11/12/1967");
        danhSachBaoVe.set(5, longBaoVe);
        
        // Kiêm tra đối tượng có tồn tại trong danh sách không
        boolean contain = danhSachMaNhanVien.contains(5);
        if (danhSachBaoVe.contains(nhatBaoVe))
            System.out.println("Danh sach co chua bao ve Nhat");
        
        
        // xoa theo đối tượng
        danhSachBaoVe.remove(longBaoVe);
        // Xóa theo index (vị trí)
        danhSachBaoVe.remove(5);
        
        for (int i = 0; i< danhSachBaoVe.size(); i++){
            BaoVe aBaoVe = danhSachBaoVe.get(i);
            System.out.println("Bao ve :" + aBaoVe.getMaNhanVien());
        }
        
        danhSachBaoVe.forEach((aBaoVe) -> {
            System.out.println("Bao ve :" + aBaoVe.getMaNhanVien());
         });
        
        danhSachBaoVe.forEach((bv)-> {
            System.out.println("Bao ve :" + bv.getMaNhanVien());
        });
    }


}


