/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuanLyNhanVien;

/**
 *
 * @author quang huy
 */
public class NhanVienKho extends NhanVienVanPhong implements WorkBenefit{
    private String tinhTrangSucKhoe;

    public NhanVienKho(String tinhTrangSucKhoe, String totNghiepTruong, String queQuan, int maNhanVien, String tenNhanVien, String ngaySinh) {
        super(totNghiepTruong, queQuan, maNhanVien, tenNhanVien, ngaySinh);
        this.tinhTrangSucKhoe = tinhTrangSucKhoe;
    }

    public String getTinhTrangSucKhoe() {
        return tinhTrangSucKhoe;
    }

    public void setTinhTrangSucKhoe(String tinhTrangSucKhoe) {
        this.tinhTrangSucKhoe = tinhTrangSucKhoe;
    }

    @Override
    public String getWorkBenefit() {
        return "Chi tra chi phi kham & chua benh.";
    }
    
}
