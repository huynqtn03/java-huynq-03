/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuanLyNhanVien;

import java.util.Date;

/**
 *
 * @author quang huy
 */
public class NhanVienVanPhong extends NhanVien{
    protected String totNghiepTruong;
    protected String queQuan;

    public NhanVienVanPhong(String totNghiepTruong, String queQuan, int maNhanVien, String tenNhanVien, String ngaySinh) {
        super(maNhanVien, tenNhanVien, ngaySinh);
        this.totNghiepTruong = totNghiepTruong;
        this.queQuan = queQuan;
    }

    public String getTotNghiepTruong() {
        return totNghiepTruong;
    }

    public void setTotNghiepTruong(String totNghiepTruong) {
        this.totNghiepTruong = totNghiepTruong;
    }

    public String getQueQuan() {
        return queQuan;
    }

    public void setQueQuan(String queQuan) {
        this.queQuan = queQuan;
    }

    
}

