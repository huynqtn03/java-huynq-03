/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayList;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author quang huy
 */
public class ToaDo {

    public int x;
    public int y;

    public ToaDo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "ToaDo{" + "x=" + x + ", y=" + y + '}';
    }

//    @Override
//    public boolean equals(Object obj) {
//        ToaDo o = (ToaDo) obj;
//
//        if (this.x != o.x || this.y != o.y) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 3;
//        hash = 37 * hash + this.x;
//        hash = 37 * hash + this.y;
//        return hash;
//    }
    public static void main(String[] args) {
        ToaDo p1 = new ToaDo(4, 5);
        ToaDo p2 = p1;
        ToaDo p3 = new ToaDo(4, 7);

        Object obj = new ToaDo(0, 0);

//        p2.x = 6;
//        p3.x=7;
        System.out.println("P1=" + p1);
        System.out.println("P2=" + p2);
        System.out.println("P3=" + p3);
        System.out.println("Obj=" + obj);

        if (p1.equals(p2)) {
            System.out.println("P1=P2");
        }
        if (p1.equals(p3)) {
            System.out.println("P1=P3");
        }
        Set<ToaDo> tapHopToaDo = new HashSet<>();
        tapHopToaDo.add(p1);
        tapHopToaDo.add(p2);
        tapHopToaDo.add(p3);
        System.out.println("Kich thuoc tap hop = " + tapHopToaDo.size());
        // có hàm equals nên p1=p2=p3=(4,5)

//        Set<ToaDo> tapHopToaDo2=tapHopToaDo;
        Set<ToaDo> tapHopToaDo2 = new HashSet<>(tapHopToaDo);
        tapHopToaDo2.remove(new ToaDo(4, 5));
        System.out.println("Kich thuoc tap hop" + tapHopToaDo);
        System.out.println("Kich thuoc tap hop" + tapHopToaDo2);
        System.out.println("Kich thuoc tap hop = " + tapHopToaDo2.size());

        int i = 5;
        int j = i;
        j = 6;
        System.out.println(i + " " + j);
    }
}
