/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayList;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author quang huy
 */

 // tạo ra danh sách 100 ptu khong trung nhau và trong khoảng 0-300
public class RandomUnique {
    public static void main(String[] args) {
        List<Integer> danhSach= new ArrayList<>(100);
        Set<Integer> tapHop= new HashSet<>();
        Random rand = new Random();
        // Đánh dấu thời gian bắt đầu xử lý
        long startTime= System.currentTimeMillis();
        
//        for(int i=0;i<100;i++){
//            danhSach.add(rand.nextInt(300)); // sẽ có các ptu trùng nhau
//        }
        while(danhSach.size()<=100){
            int randNum= rand.nextInt(300);
            if(!danhSach.contains(randNum)){
                danhSach.add(randNum);
            }
        }

        System.out.println(danhSach);
        System.out.println("Thoi gian xu ly ms="+(System.currentTimeMillis()-startTime));
        
        // Tập hợp
        startTime = System.currentTimeMillis();
        while(tapHop.size()<=100){
            int randNum= rand.nextInt(300);
            tapHop.add(randNum);
        }
        System.out.println(danhSach);
        System.out.println("Thoi gian xu ly ms="+(System.currentTimeMillis()-startTime));
        
    }
}
